module system-transparency.org/stprov

go 1.18

require (
	github.com/go-ping/ping v1.1.0
	github.com/google/uuid v1.3.0
	github.com/u-root/u-root v0.11.0
	github.com/vishvananda/netlink v1.2.1-beta.2
	golang.org/x/crypto v0.0.0-20220331220935-ae2d96664a29
	system-transparency.org/stboot v0.3.1
)

require (
	github.com/google/go-tpm v0.3.3 // indirect
	github.com/insomniacslk/dhcp v0.0.0-20230516061539-49801966e6cb // indirect
	github.com/josharian/native v1.1.0 // indirect
	github.com/mdlayher/packet v1.1.2 // indirect
	github.com/mdlayher/socket v0.4.1 // indirect
	github.com/pierrec/lz4/v4 v4.1.17 // indirect
	github.com/u-root/uio v0.0.0-20230305220412-3e8cd9d6bf63 // indirect
	github.com/vishvananda/netns v0.0.4 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sync v0.2.0 // indirect
	golang.org/x/sys v0.8.0 // indirect
)
